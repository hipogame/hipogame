package com.hipo.game.elements;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.hipo.game.util.Assets;

import java.util.Random;

public class Ball extends GameElement {

    public Ball(World world, float x, float y) {
        super(Assets.instance().getDrawable("ball_orange"));
        setName("ball");


        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        body = world.createBody(bodyDef);
        setWidth(50);
        setHeight(50);
        body.setBullet(true);

        CircleShape circle = new CircleShape();
        circle.setRadius(pixelsToBox(getWidth() * 0.5f));

        FixtureDef shapeDef = new FixtureDef();
        shapeDef.shape = circle;
        shapeDef.density = 0f;
        shapeDef.friction = 0f;
        shapeDef.restitution = 1f;

        body.createFixture(shapeDef);
        circle.dispose();
    }

    public static float generatRandomPositiveNegitiveValue(int max , int min) {
        return new Random().nextInt(max + 1 + min) - min;
    }

    public Vector2 applyImpulse() {
        int number = 3000;
        float r1 = generatRandomPositiveNegitiveValue(number,number);
        float r2 = generatRandomPositiveNegitiveValue(number,number);
        System.out.println("R1: " + r1 + " R2: " + r2);
        Vector2 vector2 = new Vector2(r1*number, r2*number);
        body.applyLinearImpulse(vector2, body.getWorldCenter(), true);
        return vector2;
    }

    public void applyImpulseRemote(float f1, float f2) {
        Vector2 v2 = new Vector2(f1, f2);
        body.applyLinearImpulse(v2, body.getWorldCenter(), true);
    }
}
