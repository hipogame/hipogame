package com.hipo.game.elements;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.hipo.game.util.Assets;

import java.util.Random;

public class Hipo extends Image {
    private Body body;
    private Animation animation;
    private float stateTime;
    private boolean looping;
    private float x;
    private float y;
    private float width, height;
    private Rectangle bounds;

    public Hipo(World world, float x, float y, boolean left) {
        Animation animation = new Animation(0.05f,
                new TextureRegion(Assets.instance().getAtlasRegion(left?"hipo_left_normal":"hipo_right_normal")),
                new TextureRegion(Assets.instance().getAtlasRegion(left?"hipo_left_eat":"hipo_right_eat")),
                new TextureRegion(Assets.instance().getAtlasRegion(left?"hipo_left_normal":"hipo_right_normal"))
        );

        this.width = 203;
        this.height = 192;
        this.x = left?x:x-width;
        this.y = y-(height/2);
        System.out.println("Place " + x+","+y+ " Size: " + width+","+height);

        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(x, y);
        body = world.createBody(bodyDef);
        setWidth(width);
        setHeight(height);
        setPosition(x, y);

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(0.1f,0.1f);

        FixtureDef shapeDef = new FixtureDef();
        shapeDef.shape = shape;
        shapeDef.density = 3f;
        shapeDef.friction = 1f;
        shapeDef.restitution = 1f;

        body.createFixture(shapeDef);
        shape.dispose();

        setAnimation(animation, false);
        setDrawable(Assets.instance().getDrawable("hipo_left_normal"));
        setSize(width,height);
        bounds = new Rectangle(this.x+(width*0.2f), this.y+(height*0.2f), width*0.6f, height*0.6f);
    }

    public void setAnimationLoop() {
        stateTime = 0;
    }

    public Rectangle getBounds() {
        return bounds;
    }

    public Body getBody() {
        return body;
    }

    private void setAnimation(Animation animation, boolean looping) {
        this.animation = animation;
        this.looping = looping;
        this.animation.setFrameDuration(0.3f);
        if (looping)
            animation.setPlayMode(Animation.PlayMode.LOOP);
        else
            animation.setPlayMode(Animation.PlayMode.NORMAL);
//        setMinWidth(Math.abs(animation.getKeyFrame(0).getRegionWidth()));
//        setMinHeight(Math.abs(animation.getKeyFrame(0).getRegionHeight()));
    }

    public boolean eats() {
        return !animation.isAnimationFinished(stateTime);
    }

    public void draw (SpriteBatch batch, float x, float y, float width, float height) {
        stateTime += Gdx.graphics.getDeltaTime();
        batch.draw(animation.getKeyFrame(stateTime, this.looping), x, y, width, height);
    }

    public void draw (SpriteBatch batch) {
        stateTime += Gdx.graphics.getDeltaTime();
        batch.draw(animation.getKeyFrame(stateTime, this.looping), x, y, width, height);
    }

    private static final float PIXELS_PER_METER = 100;

    private static float pixelsToBox(float px) {
        return px / PIXELS_PER_METER;
    }


}
