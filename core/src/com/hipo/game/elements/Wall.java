package com.hipo.game.elements;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.utils.BaseDrawable;

public class Wall extends GameElement {
    private float x1, y1, x2, y2;

    public Wall(World world, float x1, float y1, float x2, float y2) {
        super(new BaseDrawable());
        this.x1 = x1;
        this.x2 = x2;
        this.y1 = y1;
        this.y2 = y2;
        body = createThinWall(world, x1, y1, x2, y2, 0);
    }

    private static Body createThinWall(World world, float x1, float y1, float x2, float y2, float restitution) {
        // determine center point and rotation angle for createWall
        float cx = (x1 + x2) / 2;
        float cy = (y1 + y2) / 2;
        float angle = (float)Math.atan2(y2 - y1, x2 - x1);
        float mag = (float)Math.sqrt((x2 - x1) * (x2 - x1) + (y2 - y1) * (y2 - y1));
        return createWall(world, cx - mag / 2, cy - 0.05f, cx + mag / 2, cy + 0.05f, angle, restitution);
    }

    public Vector2 impulseForBall (Body ball) {
        float kick = 0.5f;
        // rotate wall direction 90 degrees for normal, choose direction toward ball
        float ix = this.y2 - this.y1;
        float iy = this.x1 - this.x2;
        float mag = (float)Math.sqrt(ix * ix + iy * iy);
        float scale = kick / mag;
        ix *= scale;
        iy *= scale;

        // dot product of (ball center - wall center) and impulse direction should be positive, if not flip impulse
        Vector2 balldiff = ball.getWorldCenter().cpy().sub(this.x1, this.y1);
        float dotprod = balldiff.x * ix + balldiff.y * iy;
        if (dotprod < 0) {
            ix = -ix;
            iy = -iy;
        }

        return new Vector2(ix, iy);
    }


    private static Body createWall(World world, float xmin, float ymin, float xmax, float ymax, float angle, float restitution) {
        float cx = (xmin + xmax) / 2;
        float cy = (ymin + ymax) / 2;
        float hx = (xmax - xmin) / 2;
        float hy = (ymax - ymin) / 2;
        if (hx < 0) hx = -hx;
        if (hy < 0) hy = -hy;
        PolygonShape wallshape = new PolygonShape();
        wallshape.setAsBox(hx, hy, new Vector2(0f, 0f), angle);

        FixtureDef fdef = new FixtureDef();
        fdef.shape = wallshape;
        fdef.density = 0f;
        if (restitution > 0) fdef.restitution = restitution;

        BodyDef bd = new BodyDef();
        bd.position.set(cx, cy);
        Body wall = world.createBody(bd);
        wall.createFixture(fdef);
        wall.setType(BodyDef.BodyType.StaticBody);
        return wall;
    }

}
