package com.hipo.game.elements;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;

public class GameElement extends Image {
    protected Body body;
    protected Rectangle bounds;

    protected GameElement(Drawable drawable) {
        super(drawable);
        bounds=new Rectangle((int)getX(), (int)getY(), (int)getWidth(), (int)getHeight());
    }

    public void setWidth (float width) {
        super.setWidth(width);
        bounds.setWidth(width*0.6f);
    }

    public void setHeight(float h) {
        super.setHeight(h);
        bounds.setHeight(h*0.6f);
    }

    protected static final float PIXELS_PER_METER = 100;

    protected static float pixelsToBox(float px) {
        return px / PIXELS_PER_METER;
    }

    public static float boxToPixels(float box) {
        return box * PIXELS_PER_METER;
    }

//    @Override
    public void setPosition(float x, float y) {
        bounds.setX(x);
        bounds.setY(y);
        super.setPosition(x, y);
    }

    public Body getBody() {
        return body;
    }

    public Rectangle getBounds() {
        return bounds;
    }
}
