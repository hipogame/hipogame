package com.hipo.game;

public interface Notificable {
    public void notificar();
}
