package com.hipo.game;

import com.badlogic.gdx.Game;
import com.hipo.game.net.Client;
import com.hipo.game.screens.EndScreen;
import com.hipo.game.screens.GameScreen;
import com.hipo.game.screens.MenuScreen;
import com.hipo.game.util.Assets;

public class MyHipoGame extends Game {
	private GameState state;
	private Client client;
	private GameScreen gameScreen;

	@Override
	public void create () {
		Assets.instance().load();
		showMenu();
	}

	public void showMenu() {
		setScreen(new MenuScreen(this));
	}

	public void showGame() {
		gameScreen = new GameScreen(this);
		state = new GameState(gameScreen);
		setScreen(gameScreen);
	}

	public boolean connect() {
		try {
			client = new Client(this);
			client.init();
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			return false;
		}
	}

	public Client getClient() {
		return client;
	}

	public GameState getState() {
		return state;
	}

	@Override
	public void render () {
		super.render();
	}

	public void showEnd() {
		setScreen(new EndScreen(this));
	}

	public GameScreen getGameScreen() {
		return gameScreen;
	}
}
