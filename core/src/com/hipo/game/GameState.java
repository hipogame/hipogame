package com.hipo.game;


import java.util.Timer;
import java.util.TimerTask;

import com.hipo.game.screens.GameScreen;

public class GameState {
    private int home;
    private int away;
    private int time;
    private Timer timer;
    private GameScreen gameScreen;

    public GameState(final GameScreen gameScreen) {
        timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                if (time > 0)
                    time--;
                else {
                    stop();
                }
            }
        }, 3, 1000);

/*
        timer.scheduleTask(new Timer.Task() {
            @Override
            public void run() {
                if (time > 0)
                    time--;
                else {
                    stop();
                }

            }
        }, 5, 1);
*/
        this.gameScreen = gameScreen;
    }

    public void start() {
        home = 0;
        away = 0;
        time = 20;
    }

    private void stop() {
        timer.cancel();
    }

    public int getTime() {
        return time;
    }

    public int getAway() {
        return away;
    }

    public int getHome() {
        return home;
    }

    public void addHome() {
        home++;
    }

    public void addAway() {
        away++;
    }

    public void setAway(int away) {
        this.away = away;
    }
}
