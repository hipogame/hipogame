package com.hipo.game.net;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Net;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.net.Socket;
import com.badlogic.gdx.net.SocketHints;
import com.hipo.game.MyHipoGame;
import com.hipo.game.Notificable;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.SocketTimeoutException;

public class Client {
    private static final short OK = 0;
    private static final short INIT = 1;
    private static final short RECONNECT = 2;
    private static final short DISCONNECT = 4;
    private static final short START = 10;
    private static final short MOVE = 11;
    private static final short DRAW = 12;
    private static final short SCORE = 14;
    private static final short ERROR = 64;

    private final static int DEFAULT_PORT = 5555;
    private final static int DEFAULT_CONTROL_PORT = 5556;
    private final MyHipoGame game;
    private Socket socket;
    private String status;
    private DataInputStream dis;
    private DataOutputStream dos;
    private long id;
    private Notificable obj;
    private Socket socketControl;
    private boolean drawBalls;
    private ControlWorker controlWorker;
    private long[] answerTimes;
    private int answerIndex;

    private static String HOST = "35.165.216.231";

    public Client(MyHipoGame game) {
        this.game = game;
    }

    public void dispose() {
        try {
            if (dis != null)
                dis.close();
            if (dos != null)
                dos.close();
        } catch (IOException ignored) {}
        if (socket != null)
            socket.dispose();
        if (controlWorker != null)
            controlWorker.interrupt();
    }

    public void init() {
        try {
            answerTimes = new long[10];
            answerIndex = 0;
            socket = Gdx.net.newClientSocket(Net.Protocol.TCP, HOST, DEFAULT_PORT, new SocketHints());

            dos = new DataOutputStream(socket.getOutputStream());
            dis = new DataInputStream(socket.getInputStream());

            dos.writeShort(INIT);
            dos.flush();
            short answer = dis.readShort();
            if (answer == ERROR) {
                status = "Protocol error.";
            } else {
                status = null;
                id = dis.readLong();
                System.out.println("Conectado con id: " + id);
            }

            System.out.println("? " + answer);
        } catch (IOException e) {
            e.printStackTrace();
            status = e.getMessage();
        }
    }

    public void submitSearch(Notificable obj) {
        this.obj = obj;
        status = "Buscando juego.";
        try {
            dos.writeShort(START);
            dos.flush();
            new ResponseWorker().start();
        } catch (IOException ioe) {
            status = ioe.getMessage();
            if (status == null)
                status = "Error desconocido";
            ioe.printStackTrace();
        }
    }

    public void disconnectFromServer() {
        try {
            dos.writeShort(DISCONNECT);
            dos.writeInt(game.getState().getHome());
            dos.flush();
            dis.readShort();
        } catch (IOException ioe) {
            status = ioe.getMessage();
            if (status == null)
                status = "Error desconocido";
            ioe.printStackTrace();
        }
    }

    public String getStatus() {
        return status;
    }

    public boolean isDrawBalls() {
        return drawBalls;
    }

    public void sendBall(Vector2 vector) {
        try {
            dos.writeShort(DRAW);
            dos.writeFloat(vector.x);
            dos.writeFloat(vector.y);
            dos.flush();
            //dis.readShort();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private void saveTime(long millis) {
        if (millis == average)
            return;

        if (answerIndex+1 < answerTimes.length) {
            answerTimes[answerIndex++] = millis;
        } else {
            System.arraycopy(answerTimes, 1, answerTimes, 0, answerTimes.length - 1);
            answerTimes[answerTimes.length-1] = millis;
        }
    }

    private long average;
    public long getAverage() {
        long acum = 0;
        int howMany = 0;
        for (long answerTime : answerTimes) {
            if (answerTime > 0) {
                howMany++;
                acum += answerTime;
            }
        }
        if (howMany > 0)
            return average = acum/howMany;
        return average = 0;
    }

    public void click() {
        try {
            long millis = System.currentTimeMillis();
            dos.writeShort(MOVE);
            dos.flush();
            //dis.readShort();
            millis = System.currentTimeMillis() - millis;
            saveTime(millis);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    public void close() {
        disconnectFromServer();
        if (controlWorker != null) {
            controlWorker.interrupt();
            try {
                controlWorker.dispose();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        dispose();
    }

    private class ControlWorker extends Thread {

        void dispose() throws IOException {
            if (dis!= null)
                dis.close();
            if (dos != null)
                dos.close();
            socketControl.dispose();
        }


        public void run() {
            DataInputStream dis = new DataInputStream(socketControl.getInputStream());
            DataOutputStream dos = new DataOutputStream(socketControl.getOutputStream());

            System.out.println("Enviando id de control: " + id);
            try {
                dos.writeLong(id);
                dos.flush();
            } catch (IOException e) {
                 e.printStackTrace();
            }

            short operation = -1;
            try {
                while (!isInterrupted() && operation != SCORE) {
                    try {
                        operation = dis.readShort();
                        System.out.println("Control Operation: " + operation);
                        switch (operation) {
                            case DRAW:
                                float f1 = dis.readFloat();
                                float f2 = dis.readFloat();
                                game.getGameScreen().drawBall(f1*-1, f2*-1);
                                break;
                            case MOVE:
                                game.getGameScreen().clickAway();
                                break;
                            case SCORE:
                                int score = dis.readInt();
                                game.getState().setAway(score);
                                break;

                        }
                        dos.writeShort(OK);
                        //System.out.println("Fin de operación");
                    } catch (SocketTimeoutException ignored) {}
                }
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    private void iniciarControl() {
        System.out.println("Iniciar control.");
        socketControl = Gdx.net.newClientSocket(Net.Protocol.TCP, HOST, DEFAULT_CONTROL_PORT, new SocketHints());
        controlWorker = new ControlWorker();
        controlWorker.start();
    }

    private class ResponseWorker extends Thread {

        public void run() {
            boolean init = false;
            try {
                drawBalls = false;
                short answer = dis.readShort();
                if (answer == ERROR) {
                    status = "Protocol error.";
                } else {
                    drawBalls = dis.readBoolean();
                    status = null;
                    System.out.println("Iniciar juego: " + id);
                    init = true;
                }
            } catch (IOException ioe) {
                status = ioe.getMessage();
                if (status == null)
                    status = "Error desconocido";
                ioe.printStackTrace();
            }
            obj.notificar();
            if (init)
                iniciarControl();
        }
    }
}
