package com.hipo.game.util;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.utils.Drawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

import java.util.HashMap;
import java.util.Map;

public class Assets {
    private static final Assets instance = new Assets();
    private Map<String, Texture> gfxMap = new HashMap<String, Texture>();
    private TextureAtlas gameAtlas;
    private BitmapFont messageFont;
    private BitmapFont casualFont;
    private BitmapFont messageFontFlipped;
    private BitmapFont casualFontFlipped;

    public static Assets instance() {
        return Assets.instance;
    }

    public void load() {
        this.messageFont = new BitmapFont(
                Gdx.files.internal("data/digital.fnt"),
                Gdx.files.internal("data/digital.png"), true);

        this.casualFont = new BitmapFont(
                Gdx.files.internal("data/casual.fnt"),
                Gdx.files.internal("data/casual.png"), true);

        this.messageFontFlipped = new BitmapFont(
                Gdx.files.internal("data/digital.fnt"),
                Gdx.files.internal("data/digital.png"), false);

        this.casualFontFlipped = new BitmapFont(
                Gdx.files.internal("data/casual.fnt"),
                Gdx.files.internal("data/casual.png"), false);

        this.gameAtlas = new TextureAtlas(Gdx.files.internal( "data/game_atlas.txt"));
    }

    public BitmapFont getMessageFont() {
        return messageFont;
    }

    public BitmapFont getCasualFont() {
        return casualFont;
    }

    public BitmapFont getMessageFontFlipped() {
        return messageFontFlipped;
    }

    public BitmapFont getCasualFontFlipped() {
        return casualFontFlipped;
    }

    public TextureAtlas.AtlasRegion getAtlasRegion(final String name) {
        return Assets.instance().gameAtlas.findRegion(name);
    }

    public TextureAtlas.AtlasRegion getAtlasRegion(final String name, int idx) {
        return Assets.instance().gameAtlas.findRegion(name, idx);
    }

    public Drawable getDrawable(final String name) {
        TextureAtlas.AtlasRegion ar =  Assets.instance().getAtlasRegion(name);
        return ( new TextureRegionDrawable( ar) );
    }

    public Drawable getDrawable(final String name, int idx) {
        TextureAtlas.AtlasRegion ar =  Assets.instance().getAtlasRegion(name, idx);
        return ( new TextureRegionDrawable( ar) );
    }

    public Texture getTexture(final String name, int idx) {
        TextureAtlas.AtlasRegion ar =  Assets.instance().getAtlasRegion(name, idx);
        return ar.getTexture();
    }

}
