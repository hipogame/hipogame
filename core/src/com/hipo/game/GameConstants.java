package com.hipo.game;

public class GameConstants {
    public static final float GAME_WIDTH = 1200;
    public static final float GAME_HEIGHT = 960;
    public static final float GAME_CENTER = GAME_WIDTH / 2;
    public static final float GAME_MIDDLE = GAME_HEIGHT / 2;


}
