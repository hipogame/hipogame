package com.hipo.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.viewport.StretchViewport;
import com.hipo.game.MyHipoGame;
import static com.hipo.game.GameConstants.*;

class AbstractScreen implements Screen {
   final Stage stage;
   private final MyHipoGame myHipoGame;
   
   AbstractScreen(MyHipoGame game) {
      this.myHipoGame = game;
      this.stage = new Stage( new StretchViewport(GAME_WIDTH, GAME_HEIGHT));
     
      Gdx.input.setInputProcessor(this.stage);
      Gdx.input.setCatchBackKey(true);
   }
   
   protected Batch getBatch() {
      return this.stage.getBatch();
   }

   protected MyHipoGame getGame() {
      return myHipoGame;
   }
   
   private boolean backClicked() {
      // return false to indicate it was not handled
      // by the screen
      return false;
   }

   @Override
   public void render(float delta) {
      Gdx.gl.glClearColor(0f, 0f, 0f, 1f);
      Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

      // update and draw the stage actors
      stage.act( delta );
      stage.draw();
   }

   @Override
   public void resize(int width, int height) {
   }
   
   public Stage getStage() {
      return this.stage;
   }

   @Override
   public void show() {
      // let this screen pocess events (like touch)
      Gdx.input.setInputProcessor( this.stage );
   }

   @Override
   public void hide() {
   }

   @Override
   public void pause() {
   }

   @Override
   public void resume() {
   }

   @Override
   public void dispose() {
      this.stage.dispose();
   }

   public float addLabel(Group menuGroup, String text, float width, Label.LabelStyle st, float yPos) {
      Label label = new Label(text, st);
      label.setWidth(width);
      label.setAlignment(Align.center);
      label.setPosition(width/2, yPos += label.getHeight() , Align.center);
      label.setFontScale(0.95f);
      menuGroup.addActor(label);
      return yPos;
   }

   public float addLabel(Group menuGroup, float width, Label label, float yPos) {
      label.setWidth(width);
      label.setAlignment(Align.center);
      label.setPosition(width/2, yPos += label.getHeight() , Align.center);
      label.setFontScale(0.95f);
      menuGroup.addActor(label);
      return yPos;
   }

}
