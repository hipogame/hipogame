package com.hipo.game.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.hipo.game.MyHipoGame;
import com.hipo.game.util.Assets;

import static com.hipo.game.GameConstants.*;

public class EndScreen extends AbstractScreen {
    private Group menuGroup;
    private float yPos;

    public EndScreen(MyHipoGame game) {
        super(game);
    }


    public void show() {
        super.show();

        Image img = new Image(Assets.instance().getDrawable("background_wood"));
        this.stage.addActor(img);

        menuGroup = new Group();
        menuGroup.setColor(Color.BLUE);
        float width = Math.max(200, GAME_WIDTH *0.5f);
        float height = Math.max(700, GAME_HEIGHT *0.5f);
        menuGroup.setSize(width, height);
        menuGroup.setPosition(GAME_CENTER, GAME_MIDDLE, Align.center);

        yPos = menuGroup.getY();
        this.stage.addActor(menuGroup);

        Label.LabelStyle st = new Label.LabelStyle(Assets.instance().getMessageFontFlipped(),  new Color(0.1f,0.6f,0.1f, 1f));
        Label.LabelStyle stGray = new Label.LabelStyle(Assets.instance().getMessageFontFlipped(),  Color.GRAY);


        yPos = addLabel(menuGroup, "Game Over", width, st, yPos);
        yPos = addLabel(menuGroup, "Results", width, st, yPos);
        yPos = addLabel(menuGroup, "Home: " + getGame().getState().getHome(), width, st, yPos);
        yPos = addLabel(menuGroup, "Away: " + getGame().getState().getAway(), width, st, yPos);
        if (getGame().getState().getHome() > getGame().getState().getAway())
            addLabel(menuGroup, "You're the winner!", width, st, yPos);
        else if (getGame().getState().getHome() < getGame().getState().getAway())
            addLabel(menuGroup, "Good luck next time!", width, stGray, yPos);


        Image buscar = new Image(Assets.instance().getDrawable("button_accept"));
        buscar.setSize(200, 200);
        buscar.setPosition(width/2, yPos += 200 , Align.center);

        buscar.addListener( new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                getGame().showMenu();
            }
        });

        menuGroup.addActor(buscar);

    }


}
