package com.hipo.game.screens;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.hipo.game.MyHipoGame;
import com.hipo.game.elements.Ball;
import com.hipo.game.elements.Hipo;
import com.hipo.game.elements.Wall;
import com.hipo.game.util.Assets;
import static com.hipo.game.GameConstants.*;

import java.util.*;

public class GameScreen extends AbstractScreen implements ContactListener {
    private static final int MAX_BALLS = 15;
    private final OrthographicCamera cam;
    private World world;
    private Timer timer;
    private List<Ball> balls;
    private float dtAccumulator;
/*
    private List<Body> wallBodies;
    private List<Body> ballBodies;
    private HashMap<Body, Wall> bodyWallHashMap;
    private HashMap<Body, Ball> bodyBallHashMap;
*/
    private Hipo hipoRight;
    private Hipo hipoLeft;
    private SpriteBatch batch;
    private ShapeRenderer renderer;
    private boolean debug = false;
    private Rectangle[] extraWalls;

    public GameScreen(MyHipoGame game) {
        super(game);

        cam = new OrthographicCamera(GAME_WIDTH, GAME_HEIGHT);

        Vector2 gravity = new Vector2(0.0f, 0.0f);
        world = new World(gravity, true);
        world.setContactListener(this);
        dtAccumulator = 0;
        balls = new ArrayList<Ball>();
/*
        ballBodies = new ArrayList<Body>();
        wallBodies = new ArrayList<Body>();
        bodyWallHashMap = new HashMap<Body, Wall>();
        bodyBallHashMap = new HashMap<Body, Ball>();
*/

        int minusW = 35;
        createWallAndStore(0, 0, 0, GAME_HEIGHT); // izquierda
        createWallAndStore(GAME_WIDTH-minusW, 0, GAME_WIDTH-minusW, GAME_HEIGHT); //derecha
        createWallAndStore(0, 0, GAME_WIDTH-minusW, 0); // top
        createWallAndStore(0, GAME_HEIGHT-30, GAME_WIDTH-minusW, GAME_HEIGHT-30); //bottom

        hipoRight = new Hipo(world, GAME_WIDTH, GAME_MIDDLE, false);
        hipoLeft = new Hipo(world, 0, GAME_MIDDLE, true);

        getStage().addActor(hipoLeft);
        getStage().addActor(hipoRight);

        batch = new SpriteBatch();
        cam.setToOrtho(true, GAME_WIDTH, GAME_HEIGHT);
        batch.setProjectionMatrix(cam.combined);

        renderer = new ShapeRenderer();
        renderer.setProjectionMatrix(cam.combined);

        extraWalls = new Rectangle[3];
        for (int i = 0; i < extraWalls.length; i++) {
            extraWalls[i] = boundsWallFromHipo(hipoRight.getBounds(), i);
        }


        getStage().addListener(new ClickListener() {
            @Override
            public void clicked(InputEvent event, float x, float y) {
                if (new Rectangle(x,y,1,1).overlaps(hipoLeft.getBounds())) {
                    //System.out.println("overlaps " + x + "," + y + " rect: " + hipoLeft.getBounds());
                    hipoLeft.setAnimationLoop();
                    getGame().getClient().click();
                }
            }
        });
    }

    private void createWallAndStore(float x1, float y1, float x2, float y2) {
        Wall wall = new Wall(world, x1, y1, x2, y2);
        getStage().addActor(wall);
/*
        wallBodies.add(wall.getBody());
        bodyWallHashMap.put(wall.getBody(), wall);
*/
    }

    public void show() {
        super.show();

        Image img = new Image(Assets.instance().getDrawable("background_wood"));
        this.stage.addActor(img);

        if (getGame().getClient().isDrawBalls()) {
            timer = new Timer();
            timer.schedule(new TimerTask() {
                @Override
                public void run() {
                    drawBall(0,0);
                }
            }, 2, 1000);
//            timer.start();
//            timer.scheduleTask( new Timer.Task() {
//            }, 1, 1);
        }

        getGame().getState().start();
    }

    protected void stop() {
        if (timer != null)
            timer.cancel();
        getGame().getClient().close();
    }

    private void updateWorld() {
        ArrayList<Ball> toRemove = new ArrayList<Ball>();
        boolean removed;
        List<Ball> copyBalls = new ArrayList<Ball>();
        copyBalls.addAll(balls);
        for (Ball ball: copyBalls) {
            removed = false;
            ball.setPosition(ball.getBody().getPosition().x, ball.getBody().getPosition().y);
            if (hipoLeft != null && hipoLeft.eats())
                if (ball.getBounds().overlaps(hipoLeft.getBounds())) {
                    toRemove.add(ball);
                    getGame().getState().addHome();
                    removed = true;
                }
            if (!removed && hipoRight != null && hipoRight.eats()) {
                if (ball.getBounds().overlaps(hipoRight.getBounds())) {
                    toRemove.add(ball);
                    getGame().getState().addAway();
                    removed = true;
                }

                if (!removed && hipoRight.eats()) {
                    int direction = getDirection(ball.getBody());
                    switch (direction) {
                        case BOTTOM_RIGHT:
                            if (ball.getBounds().overlaps(extraWalls[2])) {
                                toRemove.add(ball);
                                getGame().getState().addAway();
                                removed = true;
                            }
                            break;
                        case UP_RIGHT:
                            if (ball.getBounds().overlaps(extraWalls[0])) {
                                toRemove.add(ball);
                                getGame().getState().addAway();
                                removed = true;
                            }
                            break;
                        case BOTTOM_LEFT:
                        case UP_LEFT:
                            if (ball.getBounds().overlaps(extraWalls[1])) {
                                toRemove.add(ball);
                                getGame().getState().addAway();
                                removed = true;
                            }
                            break;
                    }

                    if (removed)
                        System.out.println("Removed by extra wall");
                }
            }


        }
        for (Ball ball: toRemove) {
            removeBall(ball);
        }
        balls.removeAll(toRemove);
    }

    private static final int UP_RIGHT = 0;
    private static final int UP_LEFT = 1;
    private static final int BOTTOM_RIGHT = 2;
    private static final int BOTTOM_LEFT = 3;

    private int getDirection(Body body) {
        Vector2 v2 = body.getLinearVelocity();
        int vel;
        if (v2.x < 0) {
            vel = v2.y > 0 ? UP_LEFT : BOTTOM_LEFT;
        } else
            vel = v2.y > 0 ? UP_RIGHT : BOTTOM_RIGHT;
        return vel;
    }

    private void removeBall(Ball ball) {
        ball.remove();
/*
        bodyBallHashMap.remove(ball.getBody());
        ballBodies.remove(ball.getBody());
*/
    }

    public void dispose() {
        super.dispose();
        timer.cancel();
    }

    public void render(float delta) {

        if (getGame().getState().getTime() < 1)
            endGame();

        cam.viewportWidth = GAME_WIDTH;
        cam.viewportHeight = GAME_HEIGHT;
        cam.position.set(GAME_WIDTH / 2, GAME_HEIGHT / 2, 0);
        cam.update();

        final int VELOCITY_ITERATIONS = 6;
        final int POSITION_ITERATIONS = 2;
        final float FIXED_TIMESTEP = 0.3f / 60.0f;
        this.dtAccumulator += delta;
        while (this.dtAccumulator > FIXED_TIMESTEP) {
            this.dtAccumulator -= FIXED_TIMESTEP;
            this.world.step(FIXED_TIMESTEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
        }

        updateWorld();
        processContacts();

        super.render(delta);

        if (hipoLeft != null) {
            batch.begin();
            hipoLeft.draw(batch);
            batch.end();
        }
        if (hipoRight != null) {
            batch.begin();
            hipoRight.draw(batch);
            batch.end();
        }


        int scoreY1 = 100;
        int scoreY2 = 150;
        int width = 200;
        int spacing = width*2;

        BitmapFont font = Assets.instance().getMessageFont();
        font.setColor(getGame().getState().getTime() > 5?Color.BLUE:Color.RED);
        batch.begin();
        font.draw(batch, "Home", GAME_CENTER-spacing, scoreY1, width, Align.center, false);
        font.draw(batch, String.valueOf(getGame().getState().getHome()), GAME_CENTER-spacing, scoreY2, width, Align.center, false);
        batch.end();

        batch.begin();
        font.draw(batch, "Time", GAME_CENTER-50, scoreY1, 100, Align.center, false);
        font.draw(batch, String.valueOf(getGame().getState().getTime()), GAME_CENTER-50, scoreY2, 100, Align.center, false);
        batch.end();


        batch.begin();
        font.draw(batch, "Away", GAME_CENTER+spacing/2, scoreY1, width, Align.center, false);
        font.draw(batch, String.valueOf(getGame().getState().getAway()), GAME_CENTER+spacing/2, scoreY2, width, Align.center, false);
        batch.end();

        /*
        *  para debug
        * */

        if (debug) {
            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.BLUE);
            renderer.rect(hipoLeft.getBounds().x, hipoLeft.getBounds().y, hipoLeft.getBounds().width, hipoLeft.getBounds().height);
            renderer.end();

            renderer.begin(ShapeRenderer.ShapeType.Line);
            renderer.setColor(Color.BLUE);
            renderer.rect(hipoRight.getBounds().x, hipoRight.getBounds().y, hipoRight.getBounds().width, hipoRight.getBounds().height);
            renderer.end();

            for (Rectangle extraWall : extraWalls) {
                renderer.begin(ShapeRenderer.ShapeType.Line);
                renderer.setColor(Color.MAROON);
                renderer.rect(extraWall.x, extraWall.y, extraWall.width, extraWall.height);
                renderer.end();
            }
        }

    }

    /* for right hipo */
    private Rectangle boundsWallFromHipo(Rectangle hipo, int type) {
        int size = 40;
        Rectangle rect = new Rectangle();
        switch (type) {
            case 0: //top
                rect.x = hipo.x - size;
                rect.height = size;
                rect.y = hipo.y - size;
                rect.width = hipo.width + size;
                break;
            case 1: //side
                rect.x = hipo.x - size;
                rect.y = hipo.y - size;
                rect.width = size;
                rect.height = hipo.height + size*2;
                break;
            case 2: //bottom
                rect.x = hipo.x - size;
                rect.y = hipo.y + hipo.height;
                rect.width = hipo.width + size;
                rect.height = size;
                break;
        }
        return rect;
    }

    private void processContacts() {
/*
        for (Body ball: ballBodies) {
            if (ball.getUserData() == null)
                continue;

            @SuppressWarnings("unchecked") List<Body> walls = (List<Body>) ball.getUserData();
            bodyBallHashMap.get(ball).remove();
            balls.remove(bodyBallHashMap.remove(ball));
            walls.clear();
        }
*/
    }

    @Override
    public void beginContact(Contact contact) {

    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }

    public void endGame() {
        stop();
        getGame().showEnd();
    }

    public void drawBall(float f1, float f2) {
        Ball ball = new Ball(world, GAME_WIDTH/2, GAME_HEIGHT/2);
        ball.setPosition(GAME_WIDTH/2, GAME_HEIGHT/2, Align.center);
/*
                ballBodies.add(ball.getBody());
                bodyBallHashMap.put(ball.getBody(), ball);
*/
        Vector2 vector = f1 == 0 && f2 == 0 ? ball.applyImpulse() : new Vector2(f1, f2);
        if (balls.size() < MAX_BALLS) {
            balls.add(ball);
            getStage().addActor(ball);
            if (getGame().getClient().isDrawBalls())
                getGame().getClient().sendBall(vector);
            else
                ball.applyImpulseRemote(f1, f2);
        }
    }

    public void clickAway() {
        hipoRight.setAnimationLoop();
    }
}
