package com.hipo.game.screens;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.scenes.scene2d.Group;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Touchable;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.utils.Align;
import com.hipo.game.MyHipoGame;
import com.hipo.game.Notificable;
import com.hipo.game.util.Assets;
import static com.hipo.game.GameConstants.*;

public class MenuScreen extends AbstractScreen implements Notificable {
    private Group menuGroup;
    private float yPos;
    private Image buscar;
    private Label looking;
    private Label error;
    Label.LabelStyle st = new Label.LabelStyle(Assets.instance().getMessageFontFlipped(),  new Color(0.1f,0.6f,0.1f, 1f));

    public MenuScreen(MyHipoGame game) {
        super(game);
    }

    public void show() {
        super.show();

        Image img = new Image(Assets.instance().getDrawable("background_hippo"));
        this.stage.addActor(img);

        menuGroup = new Group();
        menuGroup.setColor(Color.BLUE);
        float width = Math.max(200, GAME_WIDTH *0.5f);
        float height = Math.max(300, GAME_HEIGHT *0.5f);
        menuGroup.setSize(width, height);
        menuGroup.setPosition(GAME_CENTER, GAME_MIDDLE, Align.center);
        this.stage.addActor(menuGroup);

        buscar = new Image(Assets.instance().getDrawable("button_search"));
//        buscar.setWidth(300);
//        buscar.setHeight(300);
        buscar.setPosition(width/2, yPos = (height/2) + buscar.getHeight() , Align.center);

        buscar.addListener( new ClickListener() {

            @Override
            public void clicked(InputEvent event, float x, float y) {
                conectar();
            }
        });

        menuGroup.addActor(buscar);

    }

    private boolean notified;
    private void conectar() {
        if (error != null)
            error.remove();
        System.out.println("Clicked.");
        buscar.setTouchable(Touchable.disabled);
        yPos = addLabel(menuGroup, 200, looking = new Label("Looking game.", st), yPos);
        boolean conectar = getGame().connect();
        if (!conectar) {
            looking.remove();
            yPos = addLabel(menuGroup, 200, error = new Label("Conection error.", st), yPos);
            buscar.setTouchable(Touchable.enabled);
        } else {
            getGame().getClient().submitSearch(this);
            notified = false;


            Gdx.app.postRunnable(new Runnable() {
                @Override
                public void run() {
                    // Do something on the main thread
                    while (!notified) {
                        try {
                            Thread.sleep(50);
                        } catch (InterruptedException ignored) {}
                    }
                    String status = getGame().getClient().getStatus();
                    if (status == null) {
                        getGame().showGame();
                    } else {
                        buscar.setTouchable(Touchable.enabled);
                        if (error != null)
                            error.remove();
                        looking.remove();
                        yPos = addLabel(menuGroup, 200, error = new Label(status, st), buscar.getY());
                    }
                }
            });
        }
    }

    public void notificar() {
        System.out.println("Ready.");
        notified = true;
    }


}
