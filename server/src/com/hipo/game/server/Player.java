package com.hipo.game.server;

import java.net.Socket;

public class Player {
    private long id;
    private static long counter = 0;
    private boolean drawBalls;
    private boolean connected;
    private GameInstance gameInstance;
    private boolean ready;
    protected Socket socket;
    private int score;

    protected Player() {
        this.id = generatedId();
    }

    private long generatedId() {
        return counter++;
    }

    public static Player createInstance() {
        return new Player();
    }

    public void disconnected() {
        connected = false;
    }

    public boolean isOffline() {
        return !connected;
    }

    public GameInstance getGameInstance() {
        return gameInstance;
    }

    public void setGameInstance(GameInstance gameInstance) {
        this.gameInstance = gameInstance;
    }

    public boolean isReady() {
        return ready;
    }

    public void setReady(boolean ready) {
        this.ready = ready;
    }

    public void setDrawBalls(boolean drawBalls) {
        this.drawBalls = drawBalls;
    }

    public long getId() {
        return id;
    }

    public boolean isDrawBalls() {
        return drawBalls;
    }

    public Player getOpponent() {
        if (this.equals(gameInstance.getPlayer01()))
            return gameInstance.getPlayer02();
        else
            return gameInstance.getPlayer01();
    }

    public void setScore(int score) {
        this.score = score;
    }
}
