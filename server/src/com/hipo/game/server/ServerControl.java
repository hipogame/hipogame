package com.hipo.game.server;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.HashMap;

class ServerControl {
    private static final short MOVE = 11;
    private static final short DRAW = 12;

    private final int port;
    private final Server server;

    private HashMap<Player, GameControl> controls;

    ServerControl(Server server, int port) {
        this.port = port;
        this.server = server;
        controls = new HashMap<Player, GameControl>();
    }


    void start() throws IOException {
        ServerSocket ss = new ServerSocket(port);
        ss.setSoTimeout(2000);
        Socket client;
        System.out.println("Esperando clientes en puerto: " + port);
        //noinspection InfiniteLoopStatement
        while (true) {
            try {
                client = ss.accept();
                new GameControl(client).init();
            } catch (SocketTimeoutException ignored) {}
        }
    }

    void notify(Player player, int type, float x, float y) {
        GameControl gc = controls.get(player.getOpponent());
        long max = 50;
        while (gc == null && max > 0) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException ignored) {}
            gc = controls.get(player.getOpponent());
            max--;
        }

        if (gc == null) {
            System.out.println("Opponent not found.");
            return;
        }

        try {
            if (type == 0) {
                gc.move();
            } else
                gc.draw(x, y);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static byte[] lock = new byte[0];

    public void sendScore(Player player, int home) {
        GameControl gc = controls.get(player.getOpponent());
        try {
            gc.score(home);
        } catch (IOException ioe) {
            ioe.printStackTrace();
        }

    }

    private class GameControl {
        Socket client;
        DataInputStream dis;
        DataOutputStream dos;
        Player player;
        GameInstance gameInstance;

        GameControl(Socket client) {
            this.client = client;
        }

        void init() {
            try {
                dis = new DataInputStream(client.getInputStream());
                dos = new DataOutputStream(client.getOutputStream());
                long playerId = dis.readLong();
                System.out.println("Player id for control server: " + playerId);
                player = server.getPlayer(playerId);
                gameInstance = player.getGameInstance();
                controls.put(player, this);
            } catch (IOException e) {
                e.printStackTrace();
            }

        }

        void move() throws IOException {
            System.out.println("Tell to player " + player.getId() + " Move");
            dos.writeShort(Server.MOVE);
            dos.flush();
            dis.readShort();
        }

        void draw(float x, float y) throws IOException {
            System.out.println("Tell to player " + player.getId() + " DRAW " + x+","+y);
            dos.writeShort(Server.DRAW);
            dos.writeFloat(x);
            dos.writeFloat(y);
            dos.flush();
            dis.readShort();
        }

        void score(int home) throws IOException {
            dos.writeShort(Server.SCORE);
            dos.writeInt(home);
            dos.flush();
            dis.readShort();
        }
    }
}
