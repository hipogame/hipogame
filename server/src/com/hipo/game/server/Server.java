package com.hipo.game.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class Server extends Thread {
    private static final short OK = 0;
    private static final short INIT = 1;
    private static final short RECONNECT = 2;
    private static final short DISCONNECT = 4;
    private static final short START = 10;
    static final short MOVE = 11;
    static final short DRAW = 12;
    static final int SCORE = 14;
    private static final short ERROR = 64;

    private final int port;
    private HashMap<Long,Player> players;
//   private List<GameInstance> games;
    private List<GameInstance> readyGames;
    private final static byte[] lockGames = new byte[0];
    private ServerControl serverControl;

    Server(int port) {
        this.port = port;
        players = new HashMap<Long, Player>();
//        games = new ArrayList<GameInstance>();
        readyGames = new ArrayList<GameInstance>();
    }

    public void run() {
        ServerSocket ss;// = null;
        try {
            ss = new ServerSocket(port);
            ss.setSoTimeout(2000);
            Socket client;
            System.out.println("Esperando clientes en puerto: " + port);
            //noinspection InfiniteLoopStatement
            while (true) {
                try {
                    client = ss.accept();
                    new Worker(client).start();
                } catch (SocketTimeoutException ignored) {}
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private GameInstance assignGame(Player player) {
        synchronized (lockGames) {
            System.out.println("Ready games: " + readyGames.size());
            if (readyGames.size() == 0) {
                GameInstance gi = new GameInstance();
                gi.setPlayer01(player);
                player.setGameInstance(gi);
                readyGames.add(gi);
                System.out.println("Creado juego nuevo para: " + player.getId());
                return gi;
            } else {
                GameInstance gi = readyGames.remove(0);
                if (gi.getPlayer01().getId() == player.getId()) {
                    readyGames.add(gi);
                    try {
                        Thread.sleep(100);
                    } catch (InterruptedException ignored) {}
                    System.out.println("Buscando nuevamente para: " + player.getId());
                    return assignGame(player);
                } else {
                    gi.setPlayer02(player);
                    player.setGameInstance(gi);
//                    games.add(gi);
                    System.out.println("Asignado en juego completo para: " + player.getId());
                    return gi;
                }
            }
        }

    }

    Player getPlayer(long playerId) {
        return players.get(playerId);
    }

    public void setServerControl(ServerControl serverControl) {
        this.serverControl = serverControl;
    }

    private class Worker extends Thread {
        Socket client;
        DataInputStream is;
        DataOutputStream os;
        Player player;
        GameInstance gameInstance;

        Worker(Socket client) throws SocketException {
            this.client = client;
            client.setKeepAlive(true);
            client.setSoTimeout(30000);
        }

        public void run() {
            try {
                is = new DataInputStream(client.getInputStream());
                os = new DataOutputStream(client.getOutputStream());
            } catch (IOException e) {
                e.printStackTrace();
                return;
            }

            short operation;
            try {
                while (!isInterrupted()) {
                    try {
                        operation = is.readShort();
                        System.out.println("Operation: " + operation);
                        switch (operation) {
                            case INIT:
                                connectPlayer();
                                break;
                            case RECONNECT:
                                reconnect();
                                break;
                            case START:
                                startGame();
                                break;
                            case DRAW:
                                drawBall();
                                break;
                            case MOVE:
                                moveHipo();
                                break;
                            case DISCONNECT:
                                disconnectPlayer();
                                break;

                        }
                        System.out.println("Fin de operación");
                    } catch (SocketTimeoutException ignored) {}
                }
            } catch (EOFException ignored) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        void drawBall() throws IOException {
            float f1 = is.readFloat();
            float f2 = is.readFloat();
            System.out.println("Draw in: " + f1 +","+f2);
            serverControl.notify(player, 1, f1, f2);
            //os.writeShort(OK);
            //os.flush();
        }

        void moveHipo() throws IOException {
            serverControl.notify(player, 0, 0, 0);
            //os.writeShort(OK);
            //os.flush();
        }

        void startGame() throws IOException {
            gameInstance = assignGame(player);
            player.setReady(true);
            while (!gameInstance.isReady()) {
                try {
                    Thread.sleep(50);
                } catch (InterruptedException ignored) {}

            }
            os.writeShort(OK);
            os.writeBoolean(player.isDrawBalls());
            os.flush();
        }

        void reconnect() throws IOException {
            long id = is.readLong();
            Player tmp = players.get(id);
            if (tmp.isOffline()) {
                player = tmp;
                os.writeShort(OK);
                gameInstance = player.getGameInstance();
            } else
                os.writeShort(ERROR);
            os.flush();
        }

        void disconnectPlayer() throws IOException {
            int home = is.readInt();
            gameInstance.getPlayer01().setScore(home);
            serverControl.sendScore(player, home);
            os.writeShort(OK);
            if (player != null)
                player.disconnected();
        }

        void connectPlayer() throws IOException {
            player = Player.createInstance();
            os.writeShort(OK);
            os.writeLong(player.getId());
            os.flush();
            players.put(player.getId(), player);
        }
    }
}
