package com.hipo.game.server;

public class GameInstance {
    private Player player01;
    private Player player02;

    public Player getPlayer01() {
        return player01;
    }

    public void setPlayer01(Player player01) {
        this.player01 = player01;
        player01.setDrawBalls(true);
    }

    public Player getPlayer02() {
        return player02;
    }

    public void setPlayer02(Player player02) {
        this.player02 = player02;
    }

    public boolean isReady() {
        return player01 != null && player02 != null && player01.isReady() && player02.isReady();
    }
}
