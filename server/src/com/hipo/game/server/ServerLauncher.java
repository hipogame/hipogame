package com.hipo.game.server;

import java.io.IOException;

public class ServerLauncher {

    public static void main(String[] args) throws IOException {
        if (args == null || args.length < 2)
            args = new String[]{"5555","5556"};
        Server server = new Server(Integer.parseInt(args[0]));
        server.start();
        ServerControl control = new ServerControl(server, Integer.parseInt(args[1]));
        server.setServerControl(control);
        control.start();
    }

}
